import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Fungsi from './components/Fungsi/Fungsi';
import Kelas from './components/Kelas/Kelas';
import logo_react from './assets/images/logo.svg';
import NavBar from './components/NavBar/NavBar';
import gambar from './assets/images/logo.svg';
import Fungsi2 from './components/Fungsi2/Fungsi2';
import Kelas2 from './components/Kelas2/Kelas2';


const nama = "irna"
const tahun = 18
const kalimat = "semoga hari kamu menyenangkan!"
const elemen_1 = (
<div>
  <h1 className="warna">
     Halo {nama}, Selamat tanggal {tahun+1}
  </h1>
  <h2>
    {kalimat}
  </h2>
  <br/>
  <input placeholder="isi"/>
  <h2>
    Ayo belajar JS
  </h2>
</div>
);

const elemen_2 = React.createElement('h3', {className: 'warna'}, 'Hai Sayang');

const elemen_3 = React.createElement (
'footer',
{className: 'warna'},
'Terima Kasih'
);

// ReactDOM.render(
//   <React.StrictMode>
//     <NavBar/>
//     <Kelas gambar={gambar}/>
//     <Fungsi fungsi1='Fungsi Matematik' fungsi2='FUngsi Logika'  fungsi3='Fungsi Penjumlahan' />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

ReactDOM.render(
  <React.StrictMode>
    <Fungsi2/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
