import React from 'react'
import './Fungsi2.css'
import logo from '../../logo.svg';
import Kelas2 from '../Kelas2/Kelas2';

function Fungsi2(props) {
    return (
        <div className="container">
            <header className="header">
            <img src={logo} className="logoGambar" alt="logo" />
            <h2>Selamat Datang di Aplikasi React</h2>
            </header>
            <Kelas2/>
            
        </div>
    )
}

export default Fungsi2